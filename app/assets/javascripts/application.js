// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require masonry/jquery.masonry
//= require masonry/jquery.event-drag
//= require masonry/jquery.imagesloaded.min
//= require masonry/jquery.infinitescroll.min
//= require masonry/modernizr-transitions
//= require masonry/box-maker
//= require masonry/jquery.loremimages.min

$(function(){
  var $container = $('#products-container');

  $container.imagesLoaded(function(){
    $container.masonry({
      itemSelector: '.product-box',
      isAnimated: !Modernizr.csstransitions,
      isFitWidth: true
    });
  });

  // put your Uservoice code snippet in layouts/_uservoice.html.erb & use this to track clicks in GA
  // $('a#uvTabLabel').on('click', 'a', function(event){
  //   alert('uservoice');
  //   ga('send', 'event', 'Feedback', 'Left tab');
  // });

  // $('#uvTab a').click(function(event){
  //   alert('uservoice');
  //   ga('send', 'event', 'Feedback', 'Left tab');
  // });

});