$(document).ready(function() {
  
  $(".btn-tumblr").click(function() {
    ga('send', 'event', 'Product Share', 'Tumblr');
  });
  $(".btn-pinterest").click(function() {
    ga('send', 'event', 'Product Share', 'Pinterest');
  });
  $(".btn-twitter").click(function() {
    ga('send', 'event', 'Product Share', 'Twitter');
  });
  $(".btn-email").click(function() {
    ga('send', 'event', 'Product Share', 'Email');
  });
  $('#description-text a').click(function(e) {
    e.preventDefault();
  });
  $('#description-text a').each(function () {
    $(this).replaceWith($(this).text());
  });
});
