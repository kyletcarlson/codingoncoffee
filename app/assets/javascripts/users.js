$(function() {
  $('.user-lockups-table').dataTable({
    // Disable pagination since we've enabled the long y-scroll 
    "bPaginate": false,
    // Enable filtering on each column
    "bFilter": true,
    // Sorting madness!
    "bSort": true,
    // search box prefix isn't settable in here, but it lives in jquery.dataTables.js line 208
    // "sSearch": "",
    // Enable 'processing' indicator for loading large data sets.
    // "bProcessing": true,
    // Set the height of the box. overflow will enable scrollbar
    // "sScrollY": "500px",
    // Sort by the price
    "aaSorting": [[ 1, "desc" ]],  
    // Disable search on the first column. 
    // All columns must have a value or sorting breaks.
    "aoColumns": [
      { "bSearchable": false },
      null,
      null,
      null, 
      null
    ]
  });
});