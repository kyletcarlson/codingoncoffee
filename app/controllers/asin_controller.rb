class AsinController < ApplicationController
  before_action :initialize_clients
  before_action :authenticate_user!

  def seed
    authorize! :manage, Product
    items_to_process = []

    VALID_NODES.each do |n|

      # resp = @asin_client.browse_node(n, :ResponseGroup => [:BrowseNodeInfo, :TopSellers, :NewReleases, :MostGifted, :MostWishedFor]).first

      resp = @asin_client.browse_node(n, :ResponseGroup => [:TopSellers, :NewReleases]).first
      items_to_process = []

      resp.raw.TopItemSet.each do |s|
        s.TopItem.each do |i|
          puts i.ASIN
          items_to_process << i.ASIN
        end
      end

      items_to_process = items_to_process.uniq

      puts "\n\n========== \n\n"

      items_to_process.each do |asin|
        prod = @connector.lookup_by_asin(asin)
        sleep rand() + 0.8
        unless prod.nil?
          product_params = @connector.create_params(prod)
          @connector.create_or_update(product_params)
        end
      end
    end
    redirect_to root_path
  end
  
  private

    def initialize_clients
      @asin_client = ASIN::Client.instance
      @connector = Connector.new
    end
end
