class ProductsController < ApplicationController

  before_action :initialize_asin, only: [:show]
  skip_authorization_check only: [:index, :show, :buy, :search]

  respond_to :html, :json

  def index
    @products = Product.limit(200).order("RAND()").limit(25).order("sales_rank ASC")
    respond_with @products
  end

  def buy
    @product = Product.find(params[:id])
    @client = ASIN::Client.instance
    redirect_to @product.url
  end

  def show
    authorize! :show, Product
    @product = Product.find(params[:id])
    @meta_description = Sanitize.clean(@product.description[0..140]) + '...'

    # grab customer reviews <iframe> url
    resp = @client.lookup(@product.asin, ResponseGroup: :Reviews).first
    @amazon_reviews = resp.raw.CustomerReviews.first[1]

    # grab the items related to the current product
    resp = @client.similar(@product.asin, ResponseGroup: :Small, SimilarityType: :Intersection)
    if resp.any?
      related_asins = resp.collect { |p| p.asin }

      # see if there are any items we need to pull from Amazon
      @related_products = Product.where(asin: related_asins)
      existing_asins = @related_products.pluck(:asin)
      asins_to_fetch = related_asins.delete_if { |a| existing_asins.include?(a) }

      if can? :manage, Product
        # create each product we fetched that doesn't already exist
        if asins_to_fetch.any?
          conn = Connector.new
          fetched_batch = conn.fetch_batch(asins_to_fetch)
          @related_products << fetched_batch
        end
      end
      @related_products = @related_products.all.uniq.take(9)
    else
      @related_products = Product.all.order("sales_rank ASC").limit(9)
    end

    respond_with @product
  end

  def fetch
    authorize! :update, Product
    @product = Product.find(params[:id])
    conn = Connector.new
    conn.fetch_product(@product.asin)
    redirect_to @product
  end

  def new
    authorize! :create, Product
    @product = Product.new
    respond_with @product
  end

  def edit
    authorize! :edit, Product
    @product = Product.find(params[:id])
  end

  def create
    authorize! :create, Product
    @product = Product.create!(product_params)

    flash[:notice] = "Product successfully created!" if @product.save
    respond_with @product
  end

  def update
    authorize! :update, Product
    @product = Product.find(params[:id])

    flash[:notice] = "Product updated!" if @product.update_attributes(product_params)
    respond_with @product 
  end

  def destroy
    authorize! :manage, Product
    @product = Product.find(params[:id])
    @product.destroy
    flash[:notice] = "Product deleted!" if @product.destroy
    redirect_to root_path
  end

  private

    def initialize_asin
      @client = ASIN::Client.instance
    end

    def product_params
      params.require(:product).permit(:description, :image_url, :name, :price, :sku, :asin)
    end
end
