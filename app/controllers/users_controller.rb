class UsersController < ApplicationController

  before_action :authenticate_user!, :except => [:show, :lockups, :loves, :hates]
  skip_authorization_check :only => [:show, :lockups, :loves, :hates]

  respond_to :html

  def index
    authorize! :index, User, :message => "Sorry, you're not an administrator."
    @users = User.all
    redirect_to root_path
  end

  def show
    @user = User.find(params[:id])
    @lockers = Locker.lockers_with_products.where("user_id = ?", @user.id).order("RAND()")
    @lockups = Lockup.where("user_id = ?", @user.id).limit(30)
    
    # tracker = Mixpanel::Tracker.new(MIXPANEL_PROJECT_TOKEN)
    # tracker.track((current_user ? current_user.username : nil), 'User Profile View', {
    #   'Name' => @user.username,
    #   'Lockup Count' => @user.lockups.count,
    #   'User' => (current_user ? current_user.username : nil)
    # }) 
  end
end
