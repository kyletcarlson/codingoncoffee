class VotesController < ApplicationController

  before_action :authenticate_user!
  respond_to :html, :json

def create
  authorize! :create, Vote
  action = ""

  if params[:love_button]
    vote_score = 1
    action = "Love"
  else 
    vote_score = -1
    action = "Hate"
  end
  @vote = Vote.find_or_create_by!(user_id: current_user.id, product_id: params[:vote][:product_id])
  @vote.update_attributes({ score: vote_score })
  
  if @vote.save
    flash[:notice] = "Thanks for the vote!" 

    # Gabba::Gabba.new("#{GA_ACCOUNT}", "codingoncoffee.com").event("Product #{action}", "#{@vote.product.name}", "#{@vote.product.price.to_i}")

    # tracker = Mixpanel::Tracker.new(MIXPANEL_PROJECT_TOKEN)
    # tracker.track((current_user ? current_user.username : nil), "Product #{action}", {
    #   'Product Name' => @vote.product.name,
    #   'Product Price' => @vote.product.price.to_i,
    #   'Product Id' => @vote.product.id,
    #   'User' => (current_user ? current_user.username : nil)
    # }) 

  end

  redirect_to :back
end

# def destroy
#   authorize! :manage, @vote
#   @vote = Vote.find(params[:id])
#   @vote.destroy
#   redirect_to :back
# end

private

  def vote_params
    params.require(:vote).permit(:score, :user_id, :product_id)
  end
end