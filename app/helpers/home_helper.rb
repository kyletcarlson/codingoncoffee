module HomeHelper

  def signup_button_text
    version =  1 + rand(3)

    case version
    when 1
      return "Sign Up Now!"
    when 2
      return "Get Your #{APP_NAME} On!"
    when 3
      return "Create Your Account!"
    end
  end

end
