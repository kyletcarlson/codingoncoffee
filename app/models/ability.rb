class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new # guest user (not logged in)
    
    # Admins can do anything
    if user.admin?
      can :manage, :all
    else user.role == "user"
      can :create, Vote
      cannot [:edit, :update, :create, :manage], Product
      can [:show, :buy, :index], Product
    end
  end
end

