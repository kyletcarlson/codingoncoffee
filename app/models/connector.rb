class Connector
  
  def initialize
    ASIN::Configuration.configure do |config|
      config.associate_tag = 'carlscorn-20'
      config.key           = 'AKIAINHECWH7WODJB77A'
      config.secret        = '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'
    end
    @asin_client = ASIN::Client.instance
  end

  def pluck_asins(item_set)
    asin_batch = []

    item_set.each do |p|
      begin
        if p.is_a?(Array)
          asin_batch << p[1]
        else
          asin_batch << p.ASIN
        end
      rescue
        nil
      end
    end

    asin_batch
  end

  # Give it an array of nodes, and get back a flattened array the BrowseNodeIds of all of its children
  def collect_product_node_ids(nodes_to_traverse)
    collected_nodes = []

    while nodes_to_traverse.any?
      nodes_to_traverse.first[1].each do |node|
        collected_nodes << node.BrowseNodeId
        nodes_to_traverse = nodes_to_traverse.drop(1)

        if node.has_key?('Children')
          node.Children.first[1].each do |x|
            puts x.BrowseNodeId
            collected_nodes << x.BrowseNodeId
            nodes_to_traverse << x.BrowseNodeId
            nodes_to_traverse = nodes_to_traverse.drop(1)
          end
        end
      end
    end

    collected_nodes
  end
  
  # Exactly the same item lookup as what ASIN provides, but this leaves room for logging/tracking/validation/etc.
  def lookup_by_asin(asin)
    item = @asin_client.lookup(asin, ResponseGroup: :Large).first
  end

  # Scrubs the SimpleItem and generates a hash of params for creating or updating a model.
  def create_params(item)
    default_url = 'http://www.amazon.com/Best-Sellers-Books-Computer-Programming/zgbs/books/3839/?_encoding=UTF8&camp=1789&tag=carlscorn-20&creative=390957&linkCode=ur2'
    
    product_params = {
      asin: item.asin, 
      sales_rank: item.sales_rank,
      author: '',
      binding: item.binding,
      ean: item.ean,
      edition: item.edition,
      isbn: item.isbn,
      # figure out better way to handle missing attributes
      # item_height: item.item_height,
      # item_length: item.item_length,
      # item_weight: item.item_weight,
      # item_width: item.item_width,
      label: item.label,
      language: 'English',
      manufacturer: item.manufacturer,
      mpn: item.mpn,
      page_count: item.page_count,
      part_number: item.part_number,
      product_group: item.product_group,
      publication_date: item.publication_date,
      publisher: item.publisher,
      sku: item.sku,
      studio: item.studio,
      visibility: 'visible'
    }

    # chop name if it's longer than 254 characters
    if item.raw.ItemAttributes.Title.size <= 254
      product_params[:name] = item.raw.ItemAttributes.Title
    else
      product_params[:name] = item.raw.ItemAttributes.Title[0..254]
    end

    # replace url if it's longer than 254 characters
    if item.raw.has_key?('DetailPageURL') && item.raw.DetailPageURL.size <= 254
      product_params[:url] = item.raw.DetailPageURL
    else
      product_params[:url] = default_url
    end

    # check if large image
    if item.raw.has_key?('LargeImage')
      product_params[:image_url] = item.image_url
    else
      product_params[:image_url] = 'http://placehold.it/332x500'
    end

    # if there's no ItemAttributes.Amount, set price to 0
    if item.raw.ItemAttributes.has_key?('ListPrice') 
      product_params[:price] = item.amount
    else
      product_params[:price] = '0'
    end

    if item.raw.ItemAttributes.has_key?('TotalNew')
      product_params[:total_new] = item.total_new
    else
      product_params[:total_new] = '0'
    end

    if item.raw.ItemAttributes.has_key?('TotalUsed')
      product_params[:total_used] = item.total_used
    else
      product_params[:total_used] = '0'
    end

    # if multiple authors, take the first one
    if item.raw.ItemAttributes.has_key?('Author') 
      if item.raw.ItemAttributes.Author.kind_of?(Array)
        product_params[:author] = ""
        item.raw.ItemAttributes.Author.each do |a|
          product_params[:author] << a + ", "
        end
        product_params[:author] = product_params[:author][0..-3]
      else
        product_params[:author] = item.raw.ItemAttributes.Author
      end
    else
      product_params[:author] = nil
    end

    if item.raw.EditorialReviews.present?
      # if multiple "reviews", take the first one & use it as the description
      if item.raw.EditorialReviews.EditorialReview.first.kind_of?(Array)
        product_params[:description] = item.raw.EditorialReviews.EditorialReview.Content
      # only single description
      else
        product_params[:description] = item.raw.EditorialReviews.EditorialReview[0].Content
      end
    # no description information
    else
      product_params[:description] = "#{item.title} review coming soon..."
    end

    product_params
  end

  # Updates or creates a new product if we can't find one with the ASIN
  def create_or_update(product_params)
    product = Product.where("asin = ?", product_params[:asin])
    if product.empty?
      Product.create!(product_params)
    else
      product.first.update_attributes(product_params)
    end
  end

  # Takes an array of ASINs, creates/updates the records, 
  def fetch_batch(asins_to_fetch)
    product_batch = []

    fetched_products = @asin_client.lookup(asins_to_fetch.take(10))

    fetched_products.each_with_index do |p, index|
      
      p = p.raw.BrowseNodes.first

      if p[1].first.is_a?(Array)
        node_id = p[1].first[1]
      else
        node_id = p[1][0].BrowseNodeId
      end

      if VALID_NODES.include?(node_id)
        product_params = self.create_params(fetched_products[index])
        self.create_or_update(product_params)
      end
    end

    product_batch
  end


  #
  # ADMIN METHODS
  #

  # Used to manually update a product while on that page.
  def fetch_product(asin)
    item = @asin_client.lookup(asin, ResponseGroup: :Large).first
    product_params = self.create_params(item)
    self.create_or_update(product_params) 
  end

  # Same as fetch_batch(), but it doesn't return anything and doesn't check VALID_NODES constraints. Used for initial database seeding.
  def create_batch(asins_to_fetch)
    fetched_products = @asin_client.lookup(asins_to_fetch.take(10), ResponseGroup: :Large)

    fetched_products.each do |p|
      node_id = p.raw.BrowseNodes.first[1][0].BrowseNodeId

      product_params = self.create_params(p)
      self.create_or_update(product_params)
    end
  end
end