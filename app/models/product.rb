# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  asin             :string(20)
#  name             :string(255)
#  description      :text
#  price            :float
#  url              :string(255)
#  image_url        :string(255)
#  sales_rank       :integer
#  visibility       :string(255)      default("hidden")
#  created_at       :datetime
#  updated_at       :datetime
#  author           :text
#  binding          :string(255)
#  ean              :string(255)
#  edition          :string(255)
#  isbn             :string(255)
#  item_height      :string(255)
#  item_length      :string(255)
#  item_weight      :string(255)
#  item_width       :string(255)
#  label            :string(255)
#  language         :string(255)
#  formatted_price  :string(255)
#  manufacturer     :string(255)
#  mpn              :string(255)
#  page_count       :string(255)
#  part_number      :string(255)
#  product_group    :string(255)
#  publication_date :string(255)
#  publisher        :string(255)
#  sku              :string(255)
#  studio           :string(255)
#  total_new        :string(255)
#  total_used       :string(255)
#

class Product < ActiveRecord::Base
  has_many :lockups
  has_many :lockers,  :through => :lockups
  has_many :votes,    :dependent => :destroy

  validates :asin, presence: true
  validates :name, :presence => true
  validates_numericality_of :price, :allow_nil => false, :greater_than_or_equal_to => 0

  # default_scope { where("sales_rank IS NOT NULL").order("sales_rank ASC") }
end
