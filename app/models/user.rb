# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string(255)      default(""), not null
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  role                   :string(255)      default("user")
#  created_at             :datetime
#  updated_at             :datetime
#

class User < ActiveRecord::Base

  # Include default devise modules. Others available are:
  # :token_authenticatable, 
  # :lockable, :timeoutable, 
  devise :database_authenticatable, :registerable, #, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable

  # Associations
  has_many :lockers
  has_many :lockups, :through => :lockers
  has_many :inclinations
  has_many :votes

  # Validations
  validates :username,      :presence => true,
                            :uniqueness => true,
                            :length => { :minimum => 4, :maximum => 30 },
                            :format => { :with => /\A(?=.*[a-z])[a-z\d]+\Z/i }
  validates :email,         :presence => true
  validates :password,      :length => { :minimum => 8 }

  def admin?
    true if self.role == "admin"
  end

end
