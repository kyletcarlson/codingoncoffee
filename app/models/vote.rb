# == Schema Information
#
# Table name: votes
#
#  id         :integer          not null, primary key
#  score      :integer          default(0)
#  user_id    :integer
#  product_id :integer
#  created_at :datetime
#  updated_at :datetime
#

class Vote < ActiveRecord::Base
  belongs_to :product
  belongs_to :user

  validates :score, :presence => true, :allow_nil => false
  validates_numericality_of :score
  validates :product_id, :presence => true

  validates_uniqueness_of :product_id, :scope => [:user_id], :message => "You can't vote for the same product twice."

  scope :loved, -> { where("score > ?", 0) }
  scope :hated, -> { where("score < ?", 0) }
end
