require 'bundler/capistrano'

set :application, "codingoncoffee.com"
set :user, "webdog"

default_run_options[:pty] = true
# default_run_options[:shell] = '/bin/bash --login'
set :use_sudo, false # true

set :repository, "ssh://git@bitbucket.org/kyletcarlson/codingoncoffee.git"
set :scm, :git
set :branch, "master"

role :app, application
role :web, application
role :db, application, :primary => true

# set :stages, ["staging", "production"]
set :default_stage, "production"

set :deploy_to, "/home/rails"
set :deploy_via, :copy

# How many versions of your deployed code to keep
set :keep_releases, 5

# Setup Shared Folders that should be created inside the shared_path
directory_configuration = %w(db config system)

# It's good security practice to put your database.yml and other sensitive files in another folder on your server & symlink them in
desc "Symlink shared config files"
task :symlink_config_files do
  run "#{ try_sudo } ln -s #{ deploy_to }/shared/config/database.yml #{ current_path }/config/database.yml"
end

%w[start stop restart].each do |command|
  desc "#{command} Memcached"
  task command, roles: :app do
    run "service memcached #{command}"
  end
end

namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end

# http://stackoverflow.com/questions/9920564/cap-deploy-doesnt-create-share-log-folder
task :create_log_share do
  run "mkdir -p #{shared_path}/log"
end



before 'deploy:update', :create_log_share
after "deploy", "deploy:symlink_config_files"
after "deploy", "deploy:restart"
after "deploy", "deploy:cleanup"



# http://stackoverflow.com/questions/3740152/how-to-set-chmod-for-a-folder-and-all-of-its-subfolders-and-files-in-linux-ubunt

# https://gist.github.com/meskyanichi/157958

# http://guides.beanstalkapp.com/deployments/deploy-with-capistrano.html


