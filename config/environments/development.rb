FETLOCKER::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb
  # GA_ACCOUNT = "UA-46128351-5"
  # MIXPANEL_PROJECT_TOKEN = "ab63c0c27cebb5e5f3ea0c90241611ab"


  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false
  config.serve_static_assets = false

  config.eager_load = false

  # Show full error reports and disable caching
  config.consider_all_requests_local       = true
  config.action_controller.perform_caching = true
  config.cache_store = :dalli_store 

  # Set email delivery method
  config.action_mailer.default_url_options = { :host => 'localhost:3000' }
  config.action_mailer.delivery_method = :smtp
  ActionMailer::Base.default :from => 'no-reply@example.com'

  # Mandrill Config
  config.action_mailer.smtp_settings = {
    :address   => "smtp.mandrillapp.com",
    :port      => 587, # ports 587 and 2525 are also supported with STARTTLS
    :enable_starttls_auto => true, # detects and uses STARTTLS
    # :user_name => ENV["MANDRILL_USERNAME"],
    # :password  => ENV["MANDRILL_PASSWORD"], # SMTP password is any valid API key
    :user_name => "",
    :password =>  "",
    :authentication => 'plain' # Mandrill supports 'plain' or 'login'
    # :domain => 'example.com', # your domain to identify your server when connecting
  }
  # Tell the mailer to raise errors if unsuccessful
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries = true 

  config.action_mailer.default :charset => "utf-8"

  # Print deprecation notices to the Rails logger
  config.active_support.deprecation = :log

  # Only use best-standards-support built into browsers
  config.action_dispatch.best_standards_support = :builtin

  # Raise exception on mass assignment protection for Active Record models
  # config.active_record.mass_assignment_sanitizer = :strict

  # Log the query plan for queries taking more than this (works
  # with SQLite, MySQL, and PostgreSQL)
  # config.active_record.auto_explain_threshold_in_seconds = 0.5

  # Do not compress assets
  config.assets.compress = false

  # config.assets.compile = true

  # Expands the lines which load the assets
  config.assets.debug = true
end
