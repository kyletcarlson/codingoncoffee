FETLOCKER::Application.configure do
  # Settings specified here will take precedence over those in config/application.rb
  # GA_ACCOUNT = "UA-46128351-4"
  # MIXPANEL_PROJECT_TOKEN = "5b42e112fda9ea80317454be9ae13e32"
  # RAILS_DEFAULT_LOGGER = Logger.new('log/production.log')

  # Code is not reloaded between requests
  config.cache_classes = true

  config.eager_load = true

  # Full error reports are disabled and caching is turned on
  config.consider_all_requests_local = false
  config.action_controller.perform_caching = true
  config.cache_store = :dalli_store

  # Disable Rails's static asset server (Apache or nginx will already do this)
  config.serve_static_assets = false

  # Compress both stylesheets and JavaScripts
  config.assets.compress = true
  config.assets.js_compressor  = :uglifier
  # config.assets.css_compressor = :scss

  # Don't fallback to assets pipeline if a precompiled asset is missed
  config.assets.compile = true

  # Generate digests for assets URLs
  config.assets.digest = true

  # ActionMailer Config
  # Setup for production - deliveries, no errors raised
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.perform_deliveries = true
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default :charset => "utf-8"
  config.action_mailer.default_url_options = { :host => "www.example.com" }
  ActionMailer::Base.default :from => 'no-reply@example.com'

 # Mandrill Config
  config.action_mailer.smtp_settings = {
    :address   => "smtp.mandrillapp.com",
    :port      => 587, # ports 587 and 2525 are also supported with STARTTLS
    :enable_starttls_auto => true, # detects and uses STARTTLS
    :user_name => "keymaster@example.com",
    :password =>  "",
    :authentication => 'plain' # Mandrill supports 'plain' or 'login'
    # :domain => 'example.com', # your domain to identify your server when connecting
  }

  config.i18n.fallbacks = true

  config.active_support.deprecation = :notify
end
