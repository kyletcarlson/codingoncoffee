FETLOCKER::Application.routes.draw do

  # use this line for regular email/password registration
  devise_for :users, :controllers => { :registrations => "registrations" }
  
  # use this for OAuth providers via OmniAuth gem
  # devise_for :users, :controllers => { omniauth_callbacks: "omniauth_callbacks" }

  resources :users do
    resources :lockers
    resources :lockups, only: [:show, :index]
    get :loves, as: "loves"
    get :hates, as: "hates"
  end

  resources :lockers do
    resources :lockups, except: [:index]
  end

  resources :lockups, only: [:index]

  resources :products do
    member do
      get :buy
      get :list
      get :fetch
    end
  end

  resources :searches
  resources :votes, only: [:create]
  
  get "asin/seed", to: "asin#seed"

  get "sitemap.xml" => "home#sitemap", format: :xml, as: :sitemap
  get "robots.txt" => "home#robots", format: :text, as: :robots

  root :to => 'products#index'
end
