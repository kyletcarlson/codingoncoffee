# Change this to your host. See the readme at https://github.com/lassebunk/dynamic_sitemaps
# for examples of multiple hosts and folders.
host "www.codingoncoffee.com"

sitemap :site do
  url root_url, last_mod: Time.now, change_freq: "daily", priority: 1.0
end

# For products with special sitemap name and priority, and link to comments
sitemap_for Product.all, name: :all do |product|
  url product, last_mod: product.updated_at, priority: 1.0 
end