class CreateLockers < ActiveRecord::Migration
  def change
    create_table :lockers do |t|
      t.string  :name
      t.string  :description
      t.string  :visibility
      t.references :user

      t.timestamps
    end

  end
end
