class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :asin, limit: 20
      t.string :name
      t.text :description
      t.float :price
      t.string :url
      t.string :image_url
      t.integer :sales_rank
      t.references :company
      t.string :visibility, default: "hidden"
      
      t.timestamps
    end
  end
end
