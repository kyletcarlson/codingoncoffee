class CreateLockups < ActiveRecord::Migration
  def change
    create_table :lockups do |t|
      t.integer :locker_id
      t.integer :product_id
      t.integer :user_id

      t.timestamps
    end
  end
end
