class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.text :description
      t.string :url
      t.string :affiliate_tag

      t.timestamps
    end
  end
end
