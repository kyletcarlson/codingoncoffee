class AddItemAttributesToProducts < ActiveRecord::Migration
  def change
    add_column :products, :author, :text
    add_column :products, :binding, :string
    add_column :products, :ean, :string
    add_column :products, :edition, :string
    add_column :products, :isbn, :string
    add_column :products, :item_height, :string
    add_column :products, :item_length, :string
    add_column :products, :item_weight, :string
    add_column :products, :item_width, :string
    add_column :products, :label, :string
    add_column :products, :language, :string
    add_column :products, :formatted_price, :string
    add_column :products, :manufacturer, :string
    add_column :products, :mpn, :string
    add_column :products, :page_count, :string
    add_column :products, :part_number, :string
    add_column :products, :product_group, :string
    add_column :products, :publication_date, :string
    add_column :products, :publisher, :string
    add_column :products, :sku, :string
    add_column :products, :studio, :string
    add_column :products, :total_new, :string
    add_column :products, :total_used, :string
  end
end
