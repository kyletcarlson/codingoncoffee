class RemoveCompanyIdFromProducts < ActiveRecord::Migration
  def change
    remove_column :products, :company_id, :integer
  end
end
