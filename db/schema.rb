# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140123012830) do

  create_table "companies", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "url"
    t.string   "affiliate_tag"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lockers", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.string   "visibility"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lockups", force: true do |t|
    t.integer  "locker_id"
    t.integer  "product_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "asin",             limit: 20
    t.string   "name"
    t.text     "description"
    t.float    "price"
    t.string   "url"
    t.string   "image_url"
    t.integer  "sales_rank"
    t.string   "visibility",                  default: "hidden"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "author"
    t.string   "binding"
    t.string   "ean"
    t.string   "edition"
    t.string   "isbn"
    t.string   "item_height"
    t.string   "item_length"
    t.string   "item_weight"
    t.string   "item_width"
    t.string   "label"
    t.string   "language"
    t.string   "formatted_price"
    t.string   "manufacturer"
    t.string   "mpn"
    t.string   "page_count"
    t.string   "part_number"
    t.string   "product_group"
    t.string   "publication_date"
    t.string   "publisher"
    t.string   "sku"
    t.string   "studio"
    t.string   "total_new"
    t.string   "total_used"
  end

  create_table "searches", force: true do |t|
    t.string   "keywords"
    t.decimal  "min_price",  precision: 10, scale: 0
    t.decimal  "max_price",  precision: 10, scale: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username",               default: "",     null: false
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "role",                   default: "user"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "score",      default: 0
    t.integer  "user_id"
    t.integer  "product_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
