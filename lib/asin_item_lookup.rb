# XML Formatter
# http://www.freeformatter.com/xml-formatter.html#ad-output

# API Operations
# http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_OperationListAlphabetical.html

# Response information & return values
# http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_ResponseGroupsList.html
# http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_response_elements.html
# http://docs.aws.amazon.com/AWSECommerceService/latest/DG/RG_Large.html

# http://docs.aws.amazon.com/AWSECommerceService/latest/DG/USSortValuesArticle.html
# http://docs.aws.amazon.com/AWSECommerceService/latest/DG/USSearchIndexParamForItemsearch.html
# http://docs.aws.amazon.com/AWSECommerceService/latest/DG/BrowseNodeIDs.html

require 'asin'
require 'awesome_print'

ASIN::Configuration.configure do |config|
  config.associate_tag = 'carlscorn-20'
  config.key           = 'AKIAINHECWH7WODJB77A'
  config.secret        = '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'
end

# create an ASIN client
client = ASIN::Client.instance
items = client.lookup(['0321832051', '0321584104'], ResponseGroup: :Large)#.first

items.each do |item|
  if !item.nil?
    puts "\n\n==========\n\n"
    ap item.raw
    puts "\n\n==========\n\n"
    puts item.asin 
    puts item.title
    puts item.details_url   # alias the method in the gem to 'url'
    puts item.sales_rank
    puts item.review        # product.description
    puts item.image_url
    puts "$#{item.amount}"
  else
    ap "nil!"
  end
end

puts "\n\n=========\n\n"

# product_params = {
#   asin: item.asin, 
#   name: item.title,
#   description: item.review,
#   price: item.formatted_price,
#   url: item.details_url,
#   image_url: item.image_url,
#   sales_rank: item.sales_rank,
#   visibility: 'visible'
# }



# ========
# the following require .raw then the attribute.
# ========

# product URL
# puts item.raw.DetailPageURL

# 'add to wishlist', 'add to gift registry', blah
# puts item.raw.ItemLinks

# product's rank on Amazon
# puts item.raw.SalesRank
# puts "\n\n"

# small image data
# puts item.raw.SmallImage
# puts item.raw.SmallImage.Height
# puts item.raw.SmallImage.Width
# puts item.raw.SmallImage.URL

# medium image data
# puts item.raw.MediumImage
# puts item.raw.MediumImage.Height
# puts item.raw.MediumImage.Width
# puts item.raw.MediumImage.URL

# large image data
# puts item.raw.LargeImage
# puts item.raw.LargeImage.Height
# puts item.raw.LargeImage.Width
# puts item.raw.LargeImage.URL

# imagesets - primary. handle the full set: primary, swatch, variant(s) later
# puts item.raw.ImageSets.first[1][0]
# puts item.raw.ImageSets.first[1][0].SmallImage
# puts item.raw.ImageSets.first[1][0].ThumbnailImage
# puts item.raw.ImageSets.first[1][0].TinyImage
# puts item.raw.ImageSets.first[1][0].MediumImage
# puts item.raw.ImageSets.first[1][0].LargeImage
# puts item.raw.ImageSets.first[1][0].LargeImage.URL
# puts item.raw.ImageSets.first[1][0].LargeImage.Height
# puts item.raw.ImageSets.first[1][0].LargeImage.Width

# <ItemAttributes> hash
# item.raw.ItemAttributes.each do |key, array|
#   puts "#{key}\n-----"
#   puts array
#   puts "\n\n"
# end

# For books, we're concerned with:
# [:Author, :Binding, :EAN, :Edition, :ISBN, :ItemDimensions {}, :Label, :Languages [], :Languages {}, :ListPrice {}, :Manufacturer, :NumberOfPages, :PackageDimensions {}, :ProductGroup, :PublicationDate, :Publisher, :Studio, :Title]

# puts item.raw.ItemAttributes.Author
# puts item.raw.ItemAttributes.Binding
# puts item.raw.ItemAttributes.EAN
# puts item.raw.ItemAttributes.Edition  # returns an integer. Rails probably has some ordinal method to format that.
# puts item.raw.ItemAttributes.ISBN   #  == ASIN ???
# puts item.raw.ItemAttributes.ItemDimensions  # may return zeros. handle accordingly.
# puts item.raw.ItemAttributes.Label   # secondary group/authors/etc at the end of the title.
# puts item.raw.ItemAttributes.Languages.Language.first.Name
# puts item.raw.ItemAttributes.ListPrice.FormattedPrice
# puts item.raw.ItemAttributes.Manufacturer
# puts item.raw.ItemAttributes.NumberOfPages
# puts item.raw.ItemAttributes.PackageDimensions  # measured in inches. a return value of '150' is 1.5 lbs
# puts item.raw.ItemAttributes.ProductGroup   # Book, clothing, etc.
# puts item.raw.ItemAttributes.PublicationDate  # returns YYYY-MM-DD
# puts item.raw.ItemAttributes.Publisher
# puts item.raw.ItemAttributes.Studio
# puts item.raw.ItemAttributes.Title    # already mapped to a .title method in gem

# <OfferSummary> the new/used prices and amounts
# puts item.raw.OfferSummary
# item.raw.OfferSummary.each do |key, array|
#   puts "#{key}\n-----"
#   puts array
#   puts "\n\n"
# end

# <EditorialReviews> the product description
# reviews = item.raw.EditorialReviews
# ap reviews
# puts "\n\n"

# reviews.each do |r|
#   puts r
#   puts "\n"
# end

# <SimilarProducts> Duh
# puts "\n\n\n\n"
# first[1] is the array of books, while first[0] is the SimilarProducts node name.
# puts item.raw.SimilarProducts.first[1]

# item.raw.SimilarProducts.first[1].each do |p|
#   puts "-----"
#   puts p.ASIN
#   puts p.Title
# end


# <BrowseNodes> The categories the current item is in.
# ds_ps3 
# item = client.lookup('B00F6YD2AK', ResponseGroup: :Large).first.raw.BrowseNodes.first
# ds_book
# item = client.lookup('1926778898', ResponseGroup: :Large).first.raw.BrowseNodes.first

# if item[1].first.is_a?(Array)
#   puts "ARRAY"
#   puts item[1].first[1]
# else
#   puts "STRING"
#   puts item[1][0].BrowseNodeId
# end



puts "\n\n"
