require 'asin'
require 'awesome_print'

ASIN::Configuration.configure do |config|
  config.associate_tag = 'carlscorn-20'
  config.key           = 'AKIAINHECWH7WODJB77A'
  config.secret        = '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'
end

client = ASIN::Client.instance

# <BrowseNodeLookupResponse xmlns="http://webservices.amazon.com/AWSECommerceService/2011-08-01">
#   <BrowseNodes>
#     <Request>...</Request>
#     <BrowseNode>
#       <BrowseNodeId>3952</BrowseNodeId>
#       <Name>Languages & Tools</Name>
#       <TopSellers>...</TopSellers>
#       <NewReleases>...</NewReleases>  # one brief response group for each
#       <TopItemSet>...</TopItemSet>
#       <TopItemSet>...</TopItemSet>    # one detailed response group for each
#     </BrowseNode>
#   </BrowseNodes>
# </BrowseNodeLookupResponse>

# ['BrowseNodeLookupResponse']['BrowseNodes']['BrowseNode']


# MostGifted Response Group
# The MostGifted response group returns the ASINs and titles of the ten items given as gifts most within a specified browse node.

# This response group is available in all locales.
# Relevant Operations
# Operations that can use this response group include:
# • BrowseNodeLookup (p. 193)

nodes = %w[3510 3952 3839 3617 3608 3840 15375251 285856 6134006011 697342]
asins = []

nodes.each do |n|
  resp = client.browse_node(n, :ResponseGroup => [:NewReleases, :TopSellers]).first

  puts "\n\n========== \n\n"
  resp.top_sellers.each do |p|
    asins << p.ASIN
  end

  resp.new_releases.each do |p|
    asins << p.ASIN
  end
end

asins.uniq!
puts "\n\n#{asins.count} #{asins}"

