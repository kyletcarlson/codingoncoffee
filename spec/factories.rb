FactoryGirl.define do

  factory :product do
    sequence(:name) { |n| "Product 9000#{n}" }
    sequence(:description) { |n| "#{n}Lorem ipsum dolor amet. And it does cool stuff too." }
    price 100
    image_url "/images/product.jpg"
    visibility "visible"
    asin "asdf"
    url "/asdfqwerty1234.aspx"
  end

  factory :search do
    keywords "MyString"
    min_price "9.99"
    max_price "9.99"
  end

  factory :user do
    sequence(:username) { |x| "appuser#{x}" }
    sequence(:email) { |x| "appuser#{x}@gmail.com" }
    password "strongpassword"
    password_confirmation "strongpassword"
    role "user"

    # after(:create) { |user| user.send(:xyz) }

    factory :admin, parent: :user do
      role "admin"
    end
  end

  factory :vote do
    score 0 
    sequence(:user_id) { |n| n }
    sequence(:product_id) { |n| n }
  end
end