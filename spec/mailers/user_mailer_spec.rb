# # https://github.com/bmabey/email-spec

# require 'spec_helper'

# describe UserMailer do
#   let(:user) { create(:user) }
  
#   before(:each) do
#     ActionMailer::Base.delivery_method = :test
#     ActionMailer::Base.perform_deliveries = true
#     ActionMailer::Base.deliveries = []
#     UserMailer.welcome_email(user).deliver
#   end
#   after(:each) do
#     ActionMailer::Base.deliveries.clear
#   end

#   it "sends the UserMailer.welcome_email with correct headers" do
#     expect(ActionMailer::Base.deliveries.count).to eq(2)  # 2 since it BCC's me
#     expect(ActionMailer::Base.deliveries.first.from).to eq(["welcome@example.com"])
#     expect(ActionMailer::Base.deliveries.first.to).to eq([user.email])
#     expect(ActionMailer::Base.deliveries.first.bcc).to eq(["keymaster@example.com"])
#     expect(ActionMailer::Base.deliveries.first.subject).to eq("Welcome to Pinterestclone. You'll love it!")
#   end
# end
