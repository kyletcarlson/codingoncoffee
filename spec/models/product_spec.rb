# == Schema Information
#
# Table name: products
#
#  id               :integer          not null, primary key
#  asin             :string(20)
#  name             :string(255)
#  description      :text
#  price            :float
#  url              :string(255)
#  image_url        :string(255)
#  sales_rank       :integer
#  visibility       :string(255)      default("hidden")
#  created_at       :datetime
#  updated_at       :datetime
#  author           :text
#  binding          :string(255)
#  ean              :string(255)
#  edition          :string(255)
#  isbn             :string(255)
#  item_height      :string(255)
#  item_length      :string(255)
#  item_weight      :string(255)
#  item_width       :string(255)
#  label            :string(255)
#  language         :string(255)
#  formatted_price  :string(255)
#  manufacturer     :string(255)
#  mpn              :string(255)
#  page_count       :string(255)
#  part_number      :string(255)
#  product_group    :string(255)
#  publication_date :string(255)
#  publisher        :string(255)
#  sku              :string(255)
#  studio           :string(255)
#  total_new        :string(255)
#  total_used       :string(255)
#

require 'spec_helper'

describe Product do
  it "has a valid factory" do
    expect(build(:product)).to be_true
  end

  let(:product) { build(:product) }

  describe "validations" do
    it { expect(product).to validate_presence_of(:asin) }
    it { expect(product).to validate_presence_of(:name) }
    it { expect(product).to validate_numericality_of(:price) }
    it "can't have a price less than 0" do
      expect(build(:product, price: -1).save).to be_false
    end
  end

  describe "associations" do
    before(:each) do
      product.save
    end

    it { expect(product).to have_many(:votes) }
  end

  describe "scopes" do
    before(:each) do
      # @product_visible = create(:product)
      # @product_hidden  = create(:product, visibility: "hidden")
    end
  end
  
  describe "instance methods"

  describe "class methods"

end
