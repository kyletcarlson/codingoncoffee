# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  username               :string(255)      default(""), not null
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  role                   :string(255)      default("user")
#  created_at             :datetime
#  updated_at             :datetime
#

require 'spec_helper'

describe User do

  it "has a valid factory" do
    expect(build(:user)).to be_valid
  end

  let(:user) { build(:user) }

  describe "validations" do
    # it { expect(user).to validate_presence_of(:username) }
    # it { expect(user).to validate_uniqueness_of (:username) }
    # it { expect(user).to ensure_length_of(:username).is_at_least(8).is_at_most(30) }
    # it { expect(user).to allow_value("asdf124a").for(:username) }
    # it { expect(user).to_not allow_value("asd_f1!!!24a").for(:username) }
    # it { expect(user).to validate_presence_of(:email) }
    # it { expect(user).to allow_value('user@example.com').for(:email) }
    # it { expect(user).to ensure_length_of(:password).is_at_least(8) }
  end

  describe "associations" do
    it { expect(user).to have_many(:votes) }
  end

  context "instance methods" do
    it "#admin? returns true if the user's role == 'admin'" do
      admin = build(:admin)
      expect(user.admin?).to be_nil
      expect(admin.admin?).to be_true
    end
  end
end
