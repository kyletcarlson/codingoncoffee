# == Schema Information
#
# Table name: votes
#
#  id         :integer          not null, primary key
#  score      :integer          default(0)
#  user_id    :integer
#  product_id :integer
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Vote do

  it "has a valid factory" do
    expect(build(:vote)).to be_valid
  end

  let(:vote) { create(:vote) }
  let(:product) { create(:product) }

  describe "validations" do
    it { expect(vote).to validate_presence_of(:score) }
    it { expect(vote).to validate_numericality_of(:score) }
    it { expect(vote).to validate_presence_of(:product_id) }
    it { expect(vote).to validate_uniqueness_of(:product_id).scoped_to(:user_id).with_message("You can't vote for the same product twice.") }

    # same as above with the shoulda-matcher
    # it "won't allow a user to have multiple votes on a product" do
    #   vote_one = create(:vote)
    #   vote_two = build(:vote, user_id: vote_one.user_id, product_id: vote_one.product_id)
    #   expect(vote_two.save).to be_false
    # end
  end

  describe "associations" do
    it { expect(vote).to belong_to(:user) }
    it { expect(vote).to belong_to(:product) }
  end

  describe "scopes" do
    it "loved returns all votes with a score > 0" do
      product = create(:product)
      love_vote = create(:vote, score: 1, product_id: product.id)
      expect(Vote.loved.first).to eq(love_vote)
    end

    it "hated returns all votes with a score < 0" do
      product = create(:product)
      hate_vote = create(:vote, score: -1, product_id: product.id)
      expect(Vote.hated.first).to eq(hate_vote)
    end
  end

end
