# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] = 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/rspec'
require 'database_cleaner'
require 'email_spec'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  config.include(EmailSpec::Helpers)
  config.include(EmailSpec::Matchers)
  config.include Devise::TestHelpers, :type => :controller
  config.include ControllerMacros, :type => :controller 

  # Allows for shorter FactoryGirl syntax. 
  config.include FactoryGirl::Syntax::Methods
  config.treat_symbols_as_metadata_keys_with_true_values = true
  
  config.order = "random"

  # Include the Capybara DSL so that specs in spec/requests still work.
  config.include Capybara::DSL
  # Disable the old-style object.should syntax.
  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  include Warden::Test::Helpers
  Warden.test_mode!
  
  Capybara.default_host = 'localhost:3000'

  config.use_transactional_fixtures = false

  config.before(:suite) do
    DatabaseCleaner.clean_with :truncation
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  Kernel.silence_warnings {
    BCrypt::Engine::DEFAULT_COST = 0
  }

  Devise.stretches = 1
end