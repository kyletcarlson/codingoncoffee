# spec/support/request_helpers.rb
require_relative '../spec_helper'
include Warden::Test::Helpers
 
module RequestHelpers
  def create_logged_in_user
    user = Factory(:user)
    login(user)
    user
  end
 
  def login(user)    
    login_as user, scope: :user
  end
end



# # http://simple10.com/rspec-capybara-devise-login-tests/
# describe "user settings" do
#   let(:authed_user) { create_logged_in_user }
 
#   it "should allow access" do
#     visit user_settings_path(authed_user)
#     # should be good!
#   end
# end